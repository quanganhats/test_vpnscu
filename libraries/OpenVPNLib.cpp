#include "OpenVPNLib.h"


void OpenVPNLib::AddOpenVPN(std::string pathFile)
{
	std::string OpenvpnImport = "nmcli connection import type openvpn file " + pathFile;
	
	_cmdRun(OpenvpnImport);
}

void OpenVPNLib::ConnectOpenVPN(std::string vpnName)
{
	std::string OpenvpnConnect = "nmcli connection up /etc/NetworkManager/system-connections/" + vpnName + ".nmconnection";

	_cmdRun(OpenvpnConnect);
}

std::vector<std::string> OpenVPNLib::GetListVPN()
{
	std::vector<std::string> List_VPN;
	char VPNname[PATH_MAX];
	
	FILE *f_ListVPN = popen("ls /etc/NetworkManager/system-connections/", "r");
	
	if (f_ListVPN == 0)
	{
		fprintf(stderr, "Could not execute\n");
	}
	
	while (std::fgets(VPNname, PATH_MAX, f_ListVPN) != NULL)
	{
		List_VPN.push_back(VPNname);
	}
	return List_VPN;
}

bool OpenVPNLib::GetOpenVPNStatus()
{
	char StatusOpenVPN[PATH_MAX];
	char str[] = "1";

	FILE *f_StatusOpenVPN = popen("nmcli con show --active | grep -c vpn", "r");
	
	if (f_StatusOpenVPN == 0)
	{
		fprintf(stderr, "Could not execute\n");
	}
	
	fscanf(f_StatusOpenVPN,"%[^\n]",StatusOpenVPN);
	pclose(f_StatusOpenVPN);

	return (strcmp(StatusOpenVPN, str) == 0);
}

void OpenVPNLib::Disconnect()
{
	_cmdRun("killall openvpn");
}

void OpenVPNLib::RemoveOpenVPN(std::string vpnName)
{
	std::string OpenVPN_Ce_remove = "rm -rf ~/.cert/nm-openvpn/" + vpnName + "-ca.pem";
	std::string OpenVPN_Cert_remove = "rm -rf ~/.cert/nm-openvpn/" + vpnName + "-cert.pem";
	std::string OpenVPN_Key_remove = "rm -rf ~/.cert/nm-openvpn/" + vpnName + "-key.pem";
	std::string OpenVPN_remove = "rm -rf /etc/NetworkManager/system-connections/" + vpnName;

	_cmdRun(OpenVPN_remove);
	_cmdRun(OpenVPN_Cert_remove);
	_cmdRun(OpenVPN_Ce_remove);
	_cmdRun(OpenVPN_Key_remove);
}

void OpenVPNLib::_cmdRun(std::string ptm)
{
	FILE * cmdRun = popen(ptm.c_str(), "w");

	if (cmdRun == 0)
	{
		fprintf(stderr, "Could not execute\n");
	}
	
	pclose(cmdRun);
	std::fflush(stdin);
}
