#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <string.h>

#define PATH_MAX 10

namespace OpenVPNLib
{
	void AddOpenVPN(std::string pathFile);

	void ConnectOpenVPN(std::string vpnName);

	std::vector<std::string> GetListVPN();

	bool GetOpenVPNStatus();// connect => return 1. 									    Disconnect => return 0;

	void Disconnect();

	void RemoveOpenVPN(std::string vpnName);

	void _cmdRun(std::string inCommand);
}

	
