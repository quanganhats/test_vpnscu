#include "OpenVPNLib.h"

int main()
{
    int a;
    std::string pathFile, vpnName; 
    std::vector<std::string> listVPN;

    while (1)
    {
        std::cout << "nhap vao tac vu mong muon: ";  
        std::cin >> a; 

            switch (a)
    {
    case 1:
        {
            std::cout << "duong dan VPN: ";
            std::cin >> pathFile;
            OpenVPNLib::AddOpenVPN(pathFile);
        }
        
        break;

    case 2:
        {
            std::cout << "VPN connect: ";
            std::cin >> vpnName;
            OpenVPNLib::ConnectOpenVPN(vpnName);
        }
        
        break;

    case 3:
        {
            listVPN = OpenVPNLib::GetListVPN();
            for (size_t i = 0; i < listVPN.size(); i++)
            {
                std::cout << listVPN[i];
            }
            
        }
        
        break;  

    case 4:
        {
            if(OpenVPNLib::GetOpenVPNStatus())
            {
                std::cout << "VPN connected" << std::endl;
            }
            else
            {
                std::cout << "VPN don't connect" << std::endl;
            }
            
        } 
        break;

    case 5:
        {
            OpenVPNLib::Disconnect();
        }
        break;

    case 6:
        {
            std::cout << "vpn remove :";
            std::cin >> vpnName;
            OpenVPNLib::RemoveOpenVPN(vpnName);
        }
        break;

    default:
        std::cout << "thoat";
        break;
    }
    }

    return 1;

}